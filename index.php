<html lang = "hu"> 
    <head>
    <title>Administration page</title>
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        <?php
            include('session.php')
        ?>
        <nav class="navbar navbar-expand-sm bg-dark navbar-light">
            <ul class="navbar-nav mr-auto">
                <?php
                    if($_SESSION['privilige']&4) {
                ?>
                <li class="nav-item nav-link"><a class="nav-link text-light" href="admin.php">
                    Admin page
                </a></li>
                <?php
                    }if(($_SESSION['privilige']&2)||($_SESSION['privilige']&4)) {
                ?>
                <li class="nav-item nav-link"><a class="nav-link text-light" href="content.php">
                    Content maker page
                </a></li>
                <?php
                    }if($_SESSION['privilige']&1||($_SESSION['privilige']&4)) { 
                ?>
                <li class="nav-item nav-link"><a class="nav-link text-light" href="user.php">
                    Logged in user page
                </a></li>
                <?php
                    }
                ?>
            </ul>
            <a href="/logout.php"><button class="nav-item btn btn-danger">
                    Log out
            </button></a>
        </nav>
        <h1>Name:<?php echo $_SESSION['username']; ?></h1><br>
        <h2>Priviliges:<?php
            $privs = ["User","Content creator","Admin"];
            for($i=0;$i<count($privs);$i++) {
                if(1<<$i & $_SESSION['privilige'])
                    echo $privs[$i].",";
            }
        ?></h2><br>
        <h3>Last logged in: <?php echo $_SESSION['lastlog']; ?></h3>     
    </body>
</html>