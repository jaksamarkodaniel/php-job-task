<?php
   ob_start();
   session_start();
?>
<html lang = "hu"> 
   <head>
      <title>Bejelentkezés</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
      <style>
         body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #ADABAB;
         }
         
         .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
            color: #017572;
         }
         
         .form-signin .form-signin-heading,
         .form-signin .checkbox {
            margin-bottom: 10px;
         }
         
         .form-signin .checkbox {
            font-weight: normal;
         }
         
         .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
         }
         
         .form-signin .form-control:focus {
            z-index: 2;
         }
         
         .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            border-color:#017572;
         }
         
         .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-color:#017572;
         }
         
         h2{
            text-align: center;
            color: #017572;
         }
      </style>
      
   </head>
	
   <body>
      <h2>Enter Username and Password</h2> 
      <div class = "container form-signin">
         <?php
            include("config.php");

            $msg = "";
            
            if($_SERVER["REQUEST_METHOD"] == "POST") {
               // username and password sent from form 
               if (isset($_POST['login']) && !empty($_POST['username']) 
                  && !empty($_POST['password'])) {
                  $myusername = mysqli_real_escape_string($db,$_POST['username']);
                  $mypassword = md5(mysqli_real_escape_string($db,$_POST['password']));
                  
                  $sql = "SELECT * FROM users WHERE username = '$myusername' and password = '$mypassword'";
                  $result = mysqli_query($db,$sql);
                  $count = mysqli_num_rows($result);

                  if($count == 1) {
                     $row = mysqli_fetch_row($result);
                     include('session_handler.php');
                     $_SESSION['username'] = $myusername;
                     
                     $sql_upd = "UPDATE users SET LastLog=now() WHERE id=".$row[0];
                     mysqli_query($db, $sql_upd);

                     header("location: index.php");
                  }else {
                     $msg = "Your Login Name or Password is invalid" . $mypassword;
                  }
               } else {
                  $msg = "A login information is missing";
               }
            }
         ?>
      </div> <!-- /container -->
      
      <div class = "container">
         <form class = "form-signin" role = "form" 
            action = "" method = "post">
            <h4 class = "form-signin-heading"><?php echo $msg; ?></h4>
            <input type = "text" class = "form-control" 
               name = "username" placeholder = "Username" 
               required autofocus></br>
            <input type = "password" class = "form-control"
               name = "password" placeholder = "Password" required>
            <button class = "btn btn-lg btn-primary btn-block" type = "submit" 
               name = "login">Login</button>
         </form>
      </div> 
   </body>
</html>