<?php
   include('config.php');
   include('session.php');

   if(!($_SESSION['privilige']&4)) {
      header('location:index.php');
   }
?>
<html>
   <head>
   <title>Admin</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   </head>
   <body>
    <h1>Admin page!</h1><br>
    <a href="index.php"><button class="btn">Back</button></a>
   </body>
</html>